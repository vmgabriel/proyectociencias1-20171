# PROYECTO EN CONSTRUCCION

## Sistema de gestion para Mundial Futbol Rusia 2018

Universidad Distrital Francisco Jose de Caldas
Bogotá - Colombia

## Desarrolladores
* Gabriel Vargas
* Diego Hernandez
* Cristian Rios 

## Copilacion y ejecutar en Linux 
* Acceder a build `cd build`
* Ejecutar `make`
* Correr ´./proyectociencias1-2017-1´

## Copilar y ejecutar en windows 
* Accede a src
* Abre con dev c++ index.cpp 
* Presiona `F11`


## Bugs

### Windows
* Revisar compatibilida Libria string
* Revisar Directorios de ../

### Linux
