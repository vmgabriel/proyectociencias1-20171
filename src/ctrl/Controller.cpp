#include <stdio.h>
#include <iostream>
#include <sstream>

#include "../db/BaseDatos.cpp"
#include "../lib/Grupo.cpp"
#include "../lib/Equipo.cpp"
#include "../lib/Entrenador.cpp"
#include "../lib/Seleccion.cpp"

#ifndef CONTROLLER
#define CONTROLLER

class Controller
{
    private:
        int cantidadEquipos;
	    BaseDatos *db;
	    Lista<Grupo> *grupos;
	    Lista<Entrenador> *entrenadores;

	    void crearGrupos();
	    void llamareEquipos();
	    void llamarEntrenadores();
	    void llamarSeleccionEntrenador();

    public:
	    Controller(BaseDatos *);
	    void cargardb();

	    string **listaEquipos();
	    string **consultaEquiposTodos(string);
	    string **consultaEquiposGrupo(int);

	    int getCantidadEquipos();
};

// Los valores no inicializados nunca son nulos ya que estos se inicializan
// Cuando se necesitan
Controller::Controller(BaseDatos *base){this->db=base;}

void Controller::crearGrupos ()
{
    char nom='A';
    grupos=new Lista<Grupo>();
    for (int i=0;i<8;i++)
    {
        Grupo nuevo=*new Grupo(nom);
        grupos->insertarFin(nuevo);
        nom++;
    }
}

void Controller::llamareEquipos ()
{
    Equipo equipo=*new Equipo();

    string **listadatos=db->obtenerTodo(equipo.getTabla ());
    int k=0;
    //#FIXME gabriel "Debemos meter cabeza de grupo"
    for (int i=0;i<4;i++)
    {
        for (int j=0;j<8;j++)
        {
            Grupo grupotemp=grupos->seleccionar (j);
            equipo.cadena(listadatos[k]);
            grupotemp.insertarEquipo(equipo);
            grupos->modificar (j, grupotemp);
            equipo=*new Equipo();
            k++;
        }
    }
}

void Controller::llamarEntrenadores()
{
    entrenadores=new Lista<Entrenador>();
    Entrenador entrenador=*new Entrenador();
    string **listadatos=db->obtenerTodo (entrenador.getTabla ());
    for (int i=0;i<32;i++)
    {
        entrenador.cadena(listadatos[i]);
        entrenadores->insertarFin (entrenador);
        entrenador=*new Entrenador();
    }
}

void Controller::llamarSeleccionEntrenador()
{
    Seleccion seleccion = *new Seleccion();
    string **listadatos=db->obtenerTodo (seleccion.getTabla ());
    int j=0;
    for (int i=0;i<db->cantidadRegistros (seleccion.getTabla());i++)
    {
        seleccion.cadena (listadatos[i]);
        if (j+1 != seleccion.getfk ())j++;
        Entrenador entrenador=entrenadores->seleccionar (j);
        entrenador.agregarSeleccion (seleccion);
        entrenadores->modificar(j,entrenador);
        seleccion=*new Seleccion();
    }
}

void Controller::cargardb ()
{
    cout<<"Creando Grupos"<<endl;
    crearGrupos ();
    cout<<"Cargando Equipos"<<endl;
    llamareEquipos ();
    cout<<"Cargando Entrenadores"<<endl;
    llamarEntrenadores ();
    cout<<"Cargando Seleccion de Entrenadores"<<endl;
    llamarSeleccionEntrenador ();
}

string **Controller::listaEquipos ()
{
    string ** lista=new string *[32];
    Lista<Equipo> *listatodosequipos=new Lista<Equipo>();
    for (int i=0;i<8;i++)
    {
        for (int j=0;j<4;j++)
        {
            listatodosequipos->insertarFin (
                grupos->seleccionar (i).getEquipo (j));
        }
    }
    for (int i=0;i<32;i++)
    {
        stringstream  id,puesto,fk;
        string *datos=new string [5];

        id<<listatodosequipos->seleccionar(i).getId();
        puesto<<listatodosequipos->seleccionar(i).getPuesto();
        fk<<listatodosequipos->seleccionar(i).getFk();

        datos[0]=id.str();
        datos[1]=listatodosequipos->seleccionar(i).getNombre ();
        datos[2]=listatodosequipos->seleccionar(i).getConfederacion ();
        datos[3]=puesto.str();
        datos[4]=fk.str();

        lista[i]=datos;
    }
    return lista;
}

string **Controller::consultaEquiposTodos(string conf)
{
    this->cantidadEquipos=0;
    string ** lista=new string *[32];
    Lista<Equipo> *listatodosequipos=new Lista<Equipo>();
    for (int i=0;i<8;i++)
    {
        for (int j=0;j<4;j++)
        {
            listatodosequipos->insertarFin (
                grupos->seleccionar (i).getEquipo (j));
        }
    }
    for (int i=0;i<32;i++)
    {
        stringstream  id,puesto;
        string *datos=new string [6];

        id<<listatodosequipos->seleccionar(i).getId();
        puesto<<listatodosequipos->seleccionar(i).getPuesto();

        datos[0]=id.str();
        datos[1]=listatodosequipos->seleccionar(i).getNombre();
        datos[2]=listatodosequipos->seleccionar(i).getConfederacion ();
        datos[3]=puesto.str();
        Entrenador temp=*new Entrenador();
        int k=0;
        while(temp.getCed() != listatodosequipos->seleccionar (i).getFk ())
        {
            temp=entrenadores->seleccionar (k);
            k++;
        }
        datos[4]=temp.getNombre ()+" "
            +temp.getapellido ();
        datos[5]=temp.getNacionalidad ();
        if (conf=="")lista[i]=datos;//Tome todos los datos de los equipos sin discriminar
        else if (conf==datos[2]){lista[cantidadEquipos]=datos;cantidadEquipos++;}
    }
    return lista;
}

string **Controller::consultaEquiposGrupo(int val)
{
    Grupo tempgrupo=grupos->seleccionar (val);
    string **lista=new string *[tempgrupo.getcantequipos ()];
    for (int i=0;i<tempgrupo.getcantequipos ();i++)
    {
        stringstream  id,puesto;
        string *datos=new string[6];

        id<<tempgrupo.getEquipo (i).getId ();
        puesto<<tempgrupo.getEquipo (i).getPuesto ();

        datos[0]=id.str();
        datos[1]=tempgrupo.getEquipo (i).getNombre ();
        datos[2]=tempgrupo.getEquipo (i).getConfederacion ();
        datos[3]=puesto.str();

        Entrenador temp=*new Entrenador();
        int k=0;
        while(temp.getCed () != tempgrupo.getEquipo (i).getFk ())
        {
            temp=entrenadores->seleccionar (k);
            k++;
        }
        datos[4]=temp.getNombre ()+" "
            +temp.getapellido ();
        datos[5]=temp.getNacionalidad ();
        lista[i]=datos;
    }
    cantidadEquipos=tempgrupo.getcantequipos ();
    return lista;
}

int Controller::getCantidadEquipos (){return this->cantidadEquipos;}
#endif
