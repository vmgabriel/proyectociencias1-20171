#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

#ifndef DATOS
#define DATOS

class Datos
{
    private:
        void pause();

    public:
        Datos();
        void mostrarDatosEquipo(string **,int);
};

Datos::Datos(){}

//#FIXME "Modifcar para que sea practica la impresion con printf"
void Datos::mostrarDatosEquipo(string **datos,int cant)
{
    cout<<"----------------------"<<endl;
    cout<<"-Lista de Equipos-"<<endl;
    cout<<"Cod | Nombre | Confederacion | PuestoFiFa |Entrenador | NacionalidadEntrenador |"<<endl;
    cout<<"----------------------"<<endl;
    for(int i=0;i<cant;i++)
    {
        cout<<datos[i][0]<<" | ";
        cout<<datos[i][1]<<" | ";
        cout<<datos[i][2]<<" | ";
        cout<<datos[i][3]<<" | ";
        cout<<datos[i][4]<<" | ";
        cout<<datos[i][5]<<" | "<<endl;
    }
    cout<<"----------------------"<<endl;
}

#endif
