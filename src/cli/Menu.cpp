#include <stdio.h>
#include <iostream>

using namespace std;

#ifndef MENU
#define MENU

class Menu
{
    public:
        Menu();
        int menuprincipal();
        int menulistas();
        int menugrupo();
        string menuconferacion();
};

Menu::Menu(){}

int Menu::menuprincipal()
{
    cout<<"--------------------------------------------------------"<<endl;
    cout<<"------------Menu Principal----------------------"<<endl;
    cout<<"--------------------------------------------------------"<<endl;
    cout<<"1. Listas"<<endl;
    cout<<"2. Salir"<<endl;
    cout<<"--------------------------------------------------------"<<endl;
    cout<<"Ingrese el valor: ";
    int x;
    cin>>x;
    return x;
}

int Menu::menulistas()
{
    cout<<"--------------------------------------------------------"<<endl;
    cout<<"------------Menu Listas----------------------"<<endl;
    cout<<"--------------------------------------------------------"<<endl;
    cout<<"1. Lista de Jugadores Alineados en cada equipo"<<endl;
    cout<<"2. Lista de Selecciones de Cada Confederacion"<<endl;
    cout<<"3. Lista de Jugadores por equipo"<<endl;
    cout<<"4. Lista de partidos programados para fecha"<<endl;
    cout<<"5. Lista de Goleadores"<<endl;
    cout<<"6. Lista de Cada Grupo en Primera Fase"<<endl;
    cout<<"7. Arbol para segunda Fase"<<endl;
    cout<<"8. Atras"<<endl;
    cout<<"--------------------------------------------------------"<<endl;
    cout<<"Ingrese el valor: ";
    int x;
    cin>>x;
    return x;
}

string Menu::menuconferacion()
{
    cout<<"--------------------------------------------------------"<<endl;
    cout<<"------------Seleccion Confederacion----------------"<<endl;
    cout<<"---------"<<endl;
    cout<<"1. CONMEBOL (Sudamerica)"<<endl;
    cout<<"2. CONCACAF (America Norte y Centro America)"<<endl;
    cout<<"3. CAF (Africa)"<<endl;
    cout<<"4. AFC (Asia)"<<endl;
    cout<<"5. UEFA (Europa)"<<endl;
    cout<<"--------------------------------------------------------"<<endl;
    cout<<"Ingrese el valor: ";
    int x;
    cin>>x;
    switch(x)
    {
        case(1):return "CONMEBOL";
        break;
        case(2):return "CONCACAF";
        break;
        case(3):return "CAF";
        break;
        case(4):return "AFC";
        break;
        default:return "UEFA";
        break;
    }
}

int Menu::menugrupo()
{
    cout<<"--------------------------------------------------------"<<endl;
    cout<<"------------Seleccion Grupo----------------"<<endl;
    cout<<"---------"<<endl;
    cout<<"1. A"<<endl;
    cout<<"2. B"<<endl;
    cout<<"3. C"<<endl;
    cout<<"4. D"<<endl;
    cout<<"5. E"<<endl;
    cout<<"6. F"<<endl;
    cout<<"7. G"<<endl;
    cout<<"8. H"<<endl;
    cout<<"--------------------------------------------------------"<<endl;
    cout<<"Ingrese el valor: ";
    int x;
    cin>>x;
    return x;
}
#endif
