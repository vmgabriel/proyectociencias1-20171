#include <stdio.h>
#include <iostream>

using namespace std;

#include "Menu.cpp"
#include "Dialogos.cpp"
#include "../ctrl/Controller.cpp"

#ifndef INTERFAZ
#define INTERFAZ

class Interfaz
{
    private:
        Menu *menus;
        Controller *ctrl;
        Datos *dialogos;

        void bienvenida();
        void cargado();
        void fin();
        void guardado();

    public:
        Interfaz(Controller *);
        void run();
};

Interfaz::Interfaz(Controller *ctrl){this->ctrl=ctrl;}

void Interfaz::bienvenida()
{
	cout<<"--------------------------------------------------------"<<endl;
	cout<<"--Bienvenido a Mundial FIFA 2018--"<<endl;
	cout<<"---------------------------------------------------------"<<endl;
	cargado();
}

void Interfaz::cargado()
{
	cout<<"--------------------------------------------------------"<<endl;
	cout<<"--Cargando Datos de Base de Datos--"<<endl;
	cout<<"---------------------------------------------------------"<<endl;
	ctrl->cargardb ();
	cout<<"Hecho Correctamente!"<<endl;
	cout<<"---------------------------------------------------------"<<endl;
}

void Interfaz::fin()
{
    guardado();
    cout<<"--------------------------------------------------------"<<endl;
	cout<<"--Gracias, Cerrando Programa------------"<<endl;
	cout<<"---------------------------------------------------------"<<endl;
}

void Interfaz::guardado()
{
    cout<<"--------------------------------------------------------"<<endl;
	cout<<"--Guardando Datos--------------------------"<<endl;
	cout<<"---------------------------------------------------------"<<endl;
}

void Interfaz::run()
{
    bienvenida ();
    menus = new Menu();
    dialogos=new Datos();
    int opcionmenuprinci=menus->menuprincipal ();
    while (opcionmenuprinci != 2)
    {
        switch (opcionmenuprinci)
        {
            case (1):
            {
                int opcionmenusecu=menus->menulistas();
                while (opcionmenusecu != 8)
                {
                    switch (opcionmenusecu)
                    {
                        case (1):
                        {}
                        break;
                        case (2):
                        {
                            string **lista=ctrl->consultaEquiposTodos
                                (menus->menuconferacion ());
                            dialogos->mostrarDatosEquipo
                                (lista, ctrl->getCantidadEquipos ());
                        }
                        break;
                        case (3):
                        {}
                        break;
                        case (4):
                        {}
                        break;
                        case (5):
                        {}
                        break;
                        case (6):
                        {
                            string **lista=ctrl->consultaEquiposGrupo
                                (menus->menugrupo ()-1);
                            dialogos->mostrarDatosEquipo (lista,ctrl->getCantidadEquipos ());
                        }
                        break;
                        case (7):
                        {}
                        break;
                    }
                    opcionmenusecu=menus->menulistas();
                }
            }
            break;
        }
        opcionmenuprinci=menus->menuprincipal ();
    }
    guardado ();
    fin ();
}

#endif
