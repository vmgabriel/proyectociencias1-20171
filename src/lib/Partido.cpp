#include <stdio.h>
#include <iostream>
#include <string>

#include "Fecha.cpp"

#ifndef PARTIDO
#define PARTIDO

class Partido
{
    public:
        int id;
        Fecha fecha;
        int FK_Equipo2;
        int FK_Estadio;
        int FK_Resultado;
        string tabla;

    private:
        Partido();
};

Partido::Partido(){tabla="Partido";}

#endif
