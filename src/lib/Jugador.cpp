#include <stdio.h>
#include <iostream>

#include "Fecha.cpp"

#ifndef JUGADOR
#define JUGADOR

class Jugador
{
    private:
        int ced;
        string nombre;
        string apellido;
        string nacionalidad;
        int numero;
        string posicion;
        Fecha fechaNacimiento;
        double estatura;
        string club;
        int numeroGoles;
        int fechaSancion;
        bool titular;
        int fkEquipo;
        string tabla;

    public:
        Jugador();
    string getApellido() const;
    int getCed() const;
    string getClub() const;
    double getEstatura() const;
    Fecha getFechaNacimiento() const;
    int getFechaSancion() const;
    string getNacionalidad() const;
    string getNombre() const;
    int getNumero() const;
    int getNumeroGoles() const;
    string getPosicion() const;
    string getTabla() const;
    bool getTitular() const;
    void setApellido(string apellido);
    void setCed(int ced);
    void setClub(string club);
    void setEstatura(double estatura);
    void setFechaNacimiento(Fecha fechaNacimiento);
    void setFechaSancion(int fechaSancion);
    void setNacionalidad(string nacionalidad);
    void setNombre(string nombre);
    void setNumero(int numero);
    void setNumeroGoles(int numeroGoles);
    void setPosicion(string posicion);
    void setTabla(string tabla);
    void setTitular(bool titular);
};

Jugador::Jugador(){tabla="Jugador";}
string Jugador::getApellido() const
{
    return apellido;
}

int Jugador::getCed() const
{
    return ced;
}

string Jugador::getClub() const
{
    return club;
}

double Jugador::getEstatura() const
{
    return estatura;
}

Fecha Jugador::getFechaNacimiento() const
{
    return fechaNacimiento;
}

int Jugador::getFechaSancion() const
{
    return fechaSancion;
}

string Jugador::getNacionalidad() const
{
    return nacionalidad;
}

string Jugador::getNombre() const
{
    return nombre;
}

int Jugador::getNumero() const
{
    return numero;
}

int Jugador::getNumeroGoles() const
{
    return numeroGoles;
}

string Jugador::getPosicion() const
{
    return posicion;
}

string Jugador::getTabla() const
{
    return tabla;
}

bool Jugador::getTitular() const
{
    return titular;
}

void Jugador::setApellido(string apellido)
{
    this->apellido = apellido;
}

void Jugador::setCed(int ced)
{
    this->ced = ced;
}

void Jugador::setClub(string club)
{
    this->club = club;
}

void Jugador::setEstatura(double estatura)
{
    this->estatura = estatura;
}

void Jugador::setFechaNacimiento(Fecha fechaNacimiento)
{
    this->fechaNacimiento = fechaNacimiento;
}

void Jugador::setFechaSancion(int fechaSancion)
{
    this->fechaSancion = fechaSancion;
}

void Jugador::setNacionalidad(string nacionalidad)
{
    this->nacionalidad = nacionalidad;
}

void Jugador::setNombre(string nombre)
{
    this->nombre = nombre;
}

void Jugador::setNumero(int numero)
{
    this->numero = numero;
}

void Jugador::setNumeroGoles(int numeroGoles)
{
    this->numeroGoles = numeroGoles;
}

void Jugador::setPosicion(string posicion)
{
    this->posicion = posicion;
}

void Jugador::setTabla(string tabla)
{
    this->tabla = tabla;
}

void Jugador::setTitular(bool titular)
{
    this->titular = titular;
}




#endif
