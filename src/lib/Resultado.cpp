#include <stdio.h>
#include <iostream>
#include <string>

#include "Fecha.cpp"

#ifndef RESULTADO
#define RESULTADO

class Resultado
{
    private:
        int id;
        Fecha fechapartido;
        int golesequipo1;
        int golesequipo2;
        int golesuplementario1;
        int golesuplementario2;
        int golespenales1;
        int golespenales2;
        string tabla;

    public:
        Resultado();
};

Resultado::Resultado(){tabla="Resultado";}

#endif
