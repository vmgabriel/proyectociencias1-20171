#include <stdio.h>
#include <iostream>
#include <string>
#include <stdlib.h>

#include "Lista.cpp"
#include "Jugador.cpp"
#include "Partido.cpp"

#ifndef EQUIPO
#define EQUIPO

class Equipo
{
    private:
        int id;
        string nombre;
        string confederacion;
        int puestoFifa;
        int FK_entrenador;
        string tabla;
        Lista<Jugador> *convocados;
        Lista<Partido> *partidos;

    public:
        Equipo();
        void asignarDatos(int,string,string,int,int);
        void cadena(string *);

        //getter
        string getTabla();
        int getId();
        string getNombre();
        string getConfederacion();
        int getPuesto();
        int getFk();
};


Equipo::Equipo(){tabla="Equipo";}

void Equipo::asignarDatos (int id, string nom, string conf, int puest, int fk)
{
    this->id=id;
    this->nombre=nom;
    this->confederacion=conf;
    this->puestoFifa=puest;
    this->FK_entrenador=fk;
}

void Equipo::cadena (string * cadena)
{
    int id=atoi(cadena[0].c_str());
    int puesto=atoi(cadena[3].c_str ());
    int fkEntrenador=atoi (cadena[4].c_str ());
    this->asignarDatos(id,cadena[1],cadena[2],puesto,fkEntrenador);
}

string Equipo::getTabla (){return tabla;}

int Equipo::getId(){return id;}
string Equipo::getNombre(){return nombre;}
string Equipo::getConfederacion(){return confederacion;}
int Equipo::getPuesto(){return puestoFifa;}
int Equipo::getFk(){return FK_entrenador;}

#endif
