#include <stdio.h>
#include <iostream>
#include <string>

#ifndef ESTADIO
#define ESTADIO

class Estadio
{
    public:
        int id;
        string nombre;
        int capacidad;
        string ciudad;
        string tabla;

    private:
        Estadio();
};

Estadio::Estadio(){tabla="Estadio";}

#endif
